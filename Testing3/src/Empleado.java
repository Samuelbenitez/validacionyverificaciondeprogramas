enum TipoEmpleado {
	VENDEDOR, ENCARGADO

}

public class Empleado {

	public static float CalcularSaladrioBruto(TipoEmpleado tipo, float ventasMes, float horaExtras) throws Exception {

		float SalarioBruto = 0;

		if (tipo.equals(TipoEmpleado.VENDEDOR)) {

			SalarioBruto = 1000;

			if (ventasMes >= 1000 && ventasMes<1500) {
				SalarioBruto = SalarioBruto + 100;

			} else if (ventasMes >= 1500) {
				SalarioBruto = SalarioBruto + 200;
			}
			
			if (horaExtras>0) {
		    	SalarioBruto=SalarioBruto+(horaExtras*20);
				
			}

		} else if (tipo.equals(TipoEmpleado.ENCARGADO)) {
			SalarioBruto = 1500;

			if (ventasMes >= 1000) {
				SalarioBruto = SalarioBruto + 100;

			} else if (ventasMes == 1500) {
				SalarioBruto = SalarioBruto + 200;

			}
		    if (horaExtras>0) {
		    	SalarioBruto=SalarioBruto+(horaExtras*20);
				
			}


		}

		try {
			if (tipo == null || ventasMes < 0 || horaExtras < 0) {
				throw new Exception("Datos Empleado,ventas y horas extras invalidos");

			}

		} catch (Exception e) {
			throw e;

		}

		return SalarioBruto;
	}

	public static float calcularSalarioNeto(float salarioBruto) throws Exception {
		float SalarioBruto=0;

		float retenecion = 0;

		try {
			if (salarioBruto < 0) {
				throw new Exception("El monto no puede ser menor a 0");
			} else if (salarioBruto < 1000) {
				SalarioBruto=salarioBruto;

			} else if (salarioBruto >= 1000 && salarioBruto < 1500) {
				retenecion = (salarioBruto * 16) / 100;
				SalarioBruto=salarioBruto-retenecion;
				

			} else if (SalarioBruto <= 1500) {
				retenecion = (salarioBruto * 18) / 100;
				SalarioBruto=salarioBruto-retenecion;
			}

		} catch (Exception e) {

			throw e;

		}
		
      

		return SalarioBruto;
	}

}
