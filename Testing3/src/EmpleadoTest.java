import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EmpleadoTest {

	// VENDEDORPRUEBAS
	@Test
	void testCalcularSaladrioBruto1() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.VENDEDOR, 2000.0f, 8.0f);
		float resultadoEsperado = 1360.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto2() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.VENDEDOR, 1500.0f, 3.0f);
		float resultadoEsperado = 1260.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto3() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.VENDEDOR, 1499.99f, 0.0f);
		float resultadoEsperado = 1100.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	// ENCARGADOPRUEBAS

	@Test
	void testCalcularSaladrioBruto5() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.ENCARGADO, 1250.0f, 8.0f);
		float resultadoEsperado = 1760.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto6() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.ENCARGADO, 1000.0f, 0.0f);
		float resultadoEsperado = 1600.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto7() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.ENCARGADO, 999.99f, 3.0f);
		float resultadoEsperado = 1560.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto8() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.ENCARGADO, 500.0f, 0.0f);
		float resultadoEsperado = 1500.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	@Test
	void testCalcularSaladrioBruto9() throws Exception {
		float resultadoReal = Empleado.CalcularSaladrioBruto(TipoEmpleado.ENCARGADO, 0.0f, 8.0f);
		float resultadoEsperado = 1660.0f;
		assertEquals(resultadoEsperado, resultadoReal);
	}

	// EXCEPCIONES
	@Test

	void testCalcularSaladrioBruto10() throws Exception {
		Assertions.assertThrows(Exception.class,
				() -> Empleado.CalcularSaladrioBruto(TipoEmpleado.VENDEDOR, -1.0F, 8.0F));

	}

	@Test

	void testCalcularSaladrioBrut11() throws Exception {
		Assertions.assertThrows(Exception.class,
				() -> Empleado.CalcularSaladrioBruto(TipoEmpleado.VENDEDOR, 1500.0F, -1.0F));

	}

	void testCalcularSaladrioBrut12() throws Exception {
		Assertions.assertThrows(Exception.class,
				() -> Empleado.CalcularSaladrioBruto(null, 1500.0F, 8.0F));

	}

	// SALARIO NETO

	@Test
	void testCalcularSalarioNeto0() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(2000.f);
		float resultadoEsperado = 1640.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto1() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1500.0f);
		float resultadoEsperado = 1230.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto2() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1499.99f);
		float resultadoEsperado = 1259.9916f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto3() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1250.0f);
		float resultadoEsperado = 1050.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto4() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(1000.f);
		float resultadoEsperado = 840.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto5() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(999.99f);
		float resultadoEsperado = 999.99f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto6() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(500.0f);
		float resultadoEsperado = 500.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	@Test
	void testCalcularSalarioNeto7() throws Exception {
		float resultadoReal = Empleado.calcularSalarioNeto(0.0f);
		float resultadoEsperado = 0.0f;
		assertEquals(resultadoEsperado, resultadoReal);

	}

	// EXCEPCIONES

	@Test
	void testCalcularSalarioNeto8() throws Exception {
		Assertions.assertThrows(Exception.class, () -> Empleado.calcularSalarioNeto(-1.0f));

	}

}
