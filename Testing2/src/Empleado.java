
public class Empleado {

	public static float SalarioBruto;
	Empleado Vendedor = new Empleado();
	Empleado Encargado = new Empleado();
	
	
	

	float CalcularSalarioBruto(Empleado tipo, float ventasMes, float horaExtras) throws Exception {
		if (tipo.equals(Vendedor)) {
			SalarioBruto = 1000.0f;

			if (ventasMes >= 1000) {
				SalarioBruto = SalarioBruto + 100;

			} else if(ventasMes==1500){
				SalarioBruto=SalarioBruto+200;
				
			}
		} else if (tipo.equals(Encargado)) {
			SalarioBruto = 1500.0f;

		}
		try {
			if (tipo==null || ventasMes<0 || horaExtras<0) {
				throw new Exception("El tipo de empleado no existe o ventas y hora no pueden tener valors negativos");
				
			} 
			
		}catch(Exception e){
			throw e;
			
			
		}

		return SalarioBruto;

	}
	
	float calcularSalarioNeto(float salarioBruto) throws Exception {
		
		salarioBruto=SalarioBruto;
		float retenecion = 0;
		
		try {
		if (salarioBruto>=0) {
			throw new Exception("El monto no puede ser menor a 0");
		}else if(salarioBruto<1000) {
			salarioBruto=salarioBruto+0;
			
		} else if( salarioBruto==1000 && salarioBruto<1500) {
			retenecion=(salarioBruto*16)/100;
			
		} else if(salarioBruto<=1500){
			retenecion=(salarioBruto*18)/100;			
		}
		
		salarioBruto-=retenecion;
		} catch (Exception e) {
			
			throw e;
		
		}
		
		return salarioBruto;
	}
	
}
